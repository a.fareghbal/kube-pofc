# kube-pofc
Kuberentes Cluster proof of concept project

# QuickStart
```
$ git clone https://github.com/alifrg/kube-pofc.git
$ cd kube-pofc
$ vagrant up
```

# Dependencies
- ansible (Tested against version 2.8.3)
- vagrant (Tested against version 2.2.10)
- virtualbox
- docker
- a working vpn connection

# How Does it work?

## what is kube-pofc?
kube-pofc is a project with the aim of creating a highly availeble kuberentes cluster in your local environment to act as a case study.
After you run the vagrant up command you should have a kuberentes cluster with 3 masters and 3 workers ( can be changed in the vagrantfile ).

## design choices
![Alt text](https://docs.platform9.com/assets/multimaster_pmk_1260.png)

### master nodes

In order to simulate an actual high availble production ready envrionment we need to have multiple master nodes up and ready.
One of the most impornat parts to achieve it is to have an etcd cluster and becasoue of the raft algorithm and in order to avoide problems such as split brain the ideal number must be an odd one.

Note: you can also choose to create a seprate etcd cluster  to achive further issolation between the parts.

### Worker nodes
by default this projects creates 3 workers nodes but you can change this before running the vagrant up command by changing WORKERS_NUM in Vagrantfile.
It is also advised to keep at least 3 nodes running to achive High avaiabilty for your deployment.

### Loadbalancers
When running a multi-master cluster we obviously want our workers to continue working when a master fails (be manageable) but kubelet wont accept mutilple master address so the practice is to have a tcp loadbalancer in the middle to act as proxy between masters and workers.
kube-pofc uses haproxy and keepalived to achive this.

# Going through the files

The heart of this project is vagrant and ansible. to start figuring it out and customizing kube-pofc you should first take a look at the Vagrantfile on this repository. it consists of 3 main part to first create a HA loadbalancer, bootstrap the masters and then the workers with a sample application. it also has a global variable section which you can use to change the number of master and workers you want to bootstrap, the ip range the vms should have and the desired kuberentes version ( by default  V1.16.15-00).

You can also take look at src/README.md to understand how the sample application works and get a couple of ideas about CI/CD pipeline in the production environment.




# Todos
- Add Centos Support
- Add cluster Update function in provisioner