# kube-pofc application
A simple go based application
# QuickStart
```
$ git clone https://github.com/alifrg/kube-pofc.git
$ cd kube-pofc/src
$ docket build -t kube-pofc:localtest .
$ docker run -P kube-pofc:localtest
```

# Endpoints

- /v1/version (returns value of Version and Appname Envs)
- /health (dummy health check Api)

# How is it deployed on kube-pofc cluster?
when you bootstrap the kube-pofc vagrant the final part of provisoning is to use kube-pofc.yml ansible playbook and install both promethues monitroting stack and this application ( helm is used sample app).

# How should we design our pipeline in production?
One of the most important parts of today's applications is how you deploy them on the production environment so that no downtime appears and you can easily rollback. a simple pipeline should include 3 main stages build, test, and deploy. with the help of Docker and Kubernetes, we can achieve this very easily specially in the deployment part.
to deploy an application with no downtime there are different methods to be used like blue, green deployment and etc but the default Kubernetes rolling strategy with the help of its liveness and readiness probe could potentially help us to achieve it all you need to do is to create a health check API and configure the kubernetes deployment to check the state of the app against that endpoint after which Kubernetes will route the traffick only when the health check API's returns the desired values.
Also in case of a bug in application which requires to rolling back you need to understand that when using docker you should always have difrent version of your app ready to be used in registry so you can deploy them when they are needed.

# Using kuberentes to create an environment for each pr

When your team gets bigger you may need to have multiple environments for different tasks so that each of them gets tested separately and can be deployed as soon as they are ready, if you only rely on the staging env you may have to hold the pipeline for some of the task so that you can make sure everything looks good. but using kubernetes you can have multiple environments per task.
The idea is to let the developers create a complete replica of the entire stack for each of their tasks so that a particular task can be tested and deployed as soon as it's ready. you can take advantage of the Kubernetes namespace and various features to achieve this.